package com.mvp.mvptemplate.model.exception


class InternetException(message: String) : Exception(message)
